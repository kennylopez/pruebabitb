&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v9r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS C-Win 
/*------------------------------------------------------------------------

  File: 

  Description: 

  Input Parameters:
      <none>

  Output Parameters:
      <none>

  Author: 

  Created: 

------------------------------------------------------------------------*/
/*          This .W file was created with the Progress AppBuilder.      */
/*----------------------------------------------------------------------*/

/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */

CREATE WIDGET-POOL.

/* ***************************  Definitions  ************************** */
&if DEFINED(AppBuilder_is_Running) NE 0 &then
{in/vars.i NEW}
&else
{in/vars.i}
&endif

{dfvar000.i} 
{ActOpEDivisas.i} 
{dfvexcel.i} 
{dfvalidaciones.i} 
/* Parameters Definitions ---                                           */

/* Local Variable Definitions ---                                       */
DEFINE VARIABLE v-cual-pagina AS INTEGER NO-UNDO.
DEFINE VARIABLE a-ascending          AS LOGICAL EXTENT 12 INITIAL TRUE.

DEFINE VARIABLE v-continuar AS LOGICAL NO-UNDO.


DEFINE VARIABLE v-cve-subdirector  LIKE JerarquiaPromocion.promotor-subdirector NO-UNDO.
DEFINE VARIABLE vs-cve-subdirector AS INTEGER   NO-UNDO.
DEFINE VARIABLE vs-subordinados    AS LOGICAL   NO-UNDO. 
DEFINE VARIABLE vb-conJerarquia    AS LOGICAL   NO-UNDO.
DEFINE VARIABLE v-es-promotor      AS LOGICAL   NO-UNDO.
DEFINE VARIABLE v-num-promotor     AS INTEGER   NO-UNDO.
DEFINE VARIABLE v-num-promotor-lis AS CHARACTER NO-UNDO.


DEFINE BUFFER bf-Promotor  FOR Promotor.
DEFINE BUFFER bf-Jerarquia FOR JerarquiaPromocion.

DEFINE TEMP-TABLE TT-JerarquiaPromocion
    FIELD num-promotor LIKE JerarquiaPromocion.num-promotor.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE Window
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define BROWSE-NAME brw_Cliente

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES t_Cliente t-Contratos t_General t_Deal ~
t_Promotor t_Resumen

/* Definitions for BROWSE brw_Cliente                                   */
&Scoped-define FIELDS-IN-QUERY-brw_Cliente t_Cliente.cve-sucursal t_Cliente.num-cliente t_Cliente.nom-cliente t_Cliente.num-promotor t_Cliente.nom-promotor t_Cliente.num-coordinador t_Cliente.nom-coordinador t_Cliente.importe-dv t_Cliente.importe-mn t_Cliente.Ingreso-Generado   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brw_Cliente   
&Scoped-define SELF-NAME brw_Cliente
&Scoped-define QUERY-STRING-brw_Cliente FOR EACH t_Cliente NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brw_Cliente OPEN QUERY {&SELF-NAME} FOR EACH t_Cliente NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brw_Cliente t_Cliente
&Scoped-define FIRST-TABLE-IN-QUERY-brw_Cliente t_Cliente


/* Definitions for BROWSE brw_Contratos                                 */
&Scoped-define FIELDS-IN-QUERY-brw_Contratos t-Contratos.A&o t-Contratos.Num-Contratos   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brw_Contratos   
&Scoped-define SELF-NAME brw_Contratos
&Scoped-define QUERY-STRING-brw_Contratos FOR EACH t-Contratos NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brw_Contratos OPEN QUERY {&SELF-NAME} FOR EACH t-Contratos NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brw_Contratos t-Contratos
&Scoped-define FIRST-TABLE-IN-QUERY-brw_Contratos t-Contratos


/* Definitions for BROWSE brw_General                                   */
&Scoped-define FIELDS-IN-QUERY-brw_General t_General.Titulo NO-LABEL t_General.importe-dv t_General.importe-mn t_General.Ingreso-Generado   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brw_General   
&Scoped-define SELF-NAME brw_General
&Scoped-define QUERY-STRING-brw_General FOR EACH t_General NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brw_General OPEN QUERY {&SELF-NAME} FOR EACH t_General NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brw_General t_General
&Scoped-define FIRST-TABLE-IN-QUERY-brw_General t_General


/* Definitions for BROWSE brw_listado                                   */
&Scoped-define FIELDS-IN-QUERY-brw_listado t_Deal.num-deal t_Deal.cve-sucursal t_Deal.num-cliente t_Deal.nom-cliente t_Deal.Codigo-Divisa t_Deal.Tipo-Operacion t_Deal.Contraparte t_Deal.importe-dv t_Deal.importe-mn t_Deal.Ingreso-Generado t_Deal.fecha-operacion t_Deal.fecha-liquidacion t_Deal.num-folio t_Deal.tipo-deal t_Deal.q_dispersa t_Deal.estatus t_Deal.num-promotor t_Deal.nom-promotor t_Deal.producto   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brw_listado   
&Scoped-define SELF-NAME brw_listado
&Scoped-define QUERY-STRING-brw_listado FOR EACH t_Deal NO-LOCK by t_Deal.num-deal INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brw_listado OPEN QUERY {&SELF-NAME} FOR EACH t_Deal NO-LOCK by t_Deal.num-deal INDEXED-REPOSITION .
&Scoped-define TABLES-IN-QUERY-brw_listado t_Deal
&Scoped-define FIRST-TABLE-IN-QUERY-brw_listado t_Deal


/* Definitions for BROWSE brw_Promotor                                  */
&Scoped-define FIELDS-IN-QUERY-brw_Promotor t_Promotor.cve-sucursal t_Promotor.num-promotor t_Promotor.nom-promotor t_Promotor.importe-dv t_Promotor.importe-mn t_Promotor.Ingreso-Generado   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brw_Promotor   
&Scoped-define SELF-NAME brw_Promotor
&Scoped-define QUERY-STRING-brw_Promotor FOR EACH t_Promotor NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brw_Promotor OPEN QUERY {&SELF-NAME} FOR EACH t_Promotor NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brw_Promotor t_Promotor
&Scoped-define FIRST-TABLE-IN-QUERY-brw_Promotor t_Promotor


/* Definitions for BROWSE brw_Resumen                                   */
&Scoped-define FIELDS-IN-QUERY-brw_Resumen t_Resumen.A&o t_Resumen.Mes t_Resumen.importe-dv t_Resumen.importe-mn t_Resumen.Ingreso-Generado   
&Scoped-define ENABLED-FIELDS-IN-QUERY-brw_Resumen   
&Scoped-define SELF-NAME brw_Resumen
&Scoped-define QUERY-STRING-brw_Resumen FOR EACH t_Resumen NO-LOCK INDEXED-REPOSITION
&Scoped-define OPEN-QUERY-brw_Resumen OPEN QUERY {&SELF-NAME} FOR EACH t_Resumen NO-LOCK INDEXED-REPOSITION.
&Scoped-define TABLES-IN-QUERY-brw_Resumen t_Resumen
&Scoped-define FIRST-TABLE-IN-QUERY-brw_Resumen t_Resumen


/* Definitions for FRAME DEFAULT-FRAME                                  */
&Scoped-define OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME ~
    ~{&OPEN-QUERY-brw_General}

/* Definitions for FRAME FRAME-1                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-1 ~
    ~{&OPEN-QUERY-brw_listado}

/* Definitions for FRAME FRAME-2                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-2 ~
    ~{&OPEN-QUERY-brw_Cliente}

/* Definitions for FRAME FRAME-3                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-3 ~
    ~{&OPEN-QUERY-brw_Promotor}

/* Definitions for FRAME FRAME-4                                        */
&Scoped-define OPEN-BROWSERS-IN-QUERY-FRAME-4 ~
    ~{&OPEN-QUERY-brw_Contratos}~
    ~{&OPEN-QUERY-brw_Resumen}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS cb-num-promotor tgl-incluir-bajas ~
FechaInicio FechaFinal btn_Actualiza brw_General Btn_Salir fi-folder1 ~
fi-folder2 fi-folder3 fi-folder4 RECT-1 RECT-2 RECT-3 RECT-4 RECT-6 IMAGE-1 
&Scoped-Define DISPLAYED-OBJECTS cb-num-promotor tgl-incluir-bajas ~
tgl-subalternos FechaInicio vDescFechaIni FechaFinal vDescFechaFin ~
fi-folder1 fi-folder2 fi-folder3 fi-folder4 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define the widget handle for the window                              */
DEFINE VAR C-Win AS WIDGET-HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn_Actualiza 
     LABEL "Actualiza" 
     SIZE 18.72 BY 1.77 TOOLTIP "Todos"
     FONT 5.

DEFINE BUTTON btn_Reporte 
     LABEL "Reporte Ingresos eDivisas" 
     SIZE 27 BY 1.35
     FONT 1.

DEFINE BUTTON Btn_Salir AUTO-END-KEY 
     LABEL "Salir" 
     SIZE 12 BY 1
     BGCOLOR 8 FONT 5.

DEFINE VARIABLE cb-num-promotor AS INTEGER FORMAT ">>>9":U INITIAL ? 
     LABEL "Promotor" 
     VIEW-AS COMBO-BOX INNER-LINES 10
     LIST-ITEM-PAIRS "TODOS",0
     DROP-DOWN-LIST
     SIZE 55 BY 1
     BGCOLOR 15 FGCOLOR 1  NO-UNDO.

DEFINE VARIABLE FechaFinal AS DATE FORMAT "99/99/9999":U 
     LABEL "Fecha Final" 
     VIEW-AS FILL-IN 
     SIZE 9.57 BY .81
     FGCOLOR 2  NO-UNDO.

DEFINE VARIABLE FechaInicio AS DATE FORMAT "99/99/9999":U 
     LABEL "Fecha Inicio" 
     VIEW-AS FILL-IN 
     SIZE 9.57 BY .81
     FGCOLOR 2  NO-UNDO.

DEFINE VARIABLE fi-folder1 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 22 BY .62
     BGCOLOR 8 FONT 9 NO-UNDO.

DEFINE VARIABLE fi-folder2 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 23.14 BY .62
     FONT 9 NO-UNDO.

DEFINE VARIABLE fi-folder3 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 22 BY .62
     FONT 9 NO-UNDO.

DEFINE VARIABLE fi-folder4 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 35.29 BY .62
     FONT 9 NO-UNDO.

DEFINE VARIABLE vDescFechaFin AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 48 BY .81
     BGCOLOR 15 FGCOLOR 2  NO-UNDO.

DEFINE VARIABLE vDescFechaIni AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 48 BY .81
     BGCOLOR 15 FGCOLOR 2  NO-UNDO.

DEFINE IMAGE IMAGE-1
     FILENAME "ICO/vector72.png":U TRANSPARENT
     SIZE 11 BY 2.96.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24 BY .92
     BGCOLOR 3 FGCOLOR 3 .

DEFINE RECTANGLE RECT-2
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 24.86 BY .92.

DEFINE RECTANGLE RECT-3
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 26.72 BY .92
     BGCOLOR 3 FGCOLOR 3 .

DEFINE RECTANGLE RECT-4
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 37 BY .92.

DEFINE RECTANGLE RECT-6
     EDGE-PIXELS 6 GRAPHIC-EDGE  NO-FILL   
     SIZE 156.57 BY .5.

DEFINE VARIABLE tgl-incluir-bajas AS LOGICAL INITIAL no 
     LABEL "Incluir pomotores de baja" 
     VIEW-AS TOGGLE-BOX
     SIZE 20 BY .81 NO-UNDO.

DEFINE VARIABLE tgl-subalternos AS LOGICAL INITIAL no 
     LABEL "Todos sus subalternos" 
     VIEW-AS TOGGLE-BOX
     SIZE 18 BY .77 NO-UNDO.

DEFINE BUTTON btn_excel 
     LABEL "Excel" 
     SIZE 15 BY 1.15 TOOLTIP "Reporte a excel".

DEFINE BUTTON btn_contratos 
     LABEL "Contratos" 
     SIZE 15 BY 1.15 TOOLTIP "Reporte de Contratos".

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY brw_Cliente FOR 
      t_Cliente SCROLLING.

DEFINE QUERY brw_Contratos FOR 
      t-Contratos SCROLLING.

DEFINE QUERY brw_General FOR 
      t_General SCROLLING.

DEFINE QUERY brw_listado FOR 
      t_Deal SCROLLING.

DEFINE QUERY brw_Promotor FOR 
      t_Promotor SCROLLING.

DEFINE QUERY brw_Resumen FOR 
      t_Resumen SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE brw_Cliente
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brw_Cliente C-Win _FREEFORM
  QUERY brw_Cliente DISPLAY
      t_Cliente.cve-sucursal         FORMAT "X(3)":U                
t_Cliente.num-cliente          FORMAT ">>>>>9":U        
t_Cliente.nom-cliente          FORMAT "X(40)":U  
t_Cliente.num-promotor         FORMAT ">>>>9"
t_Cliente.nom-promotor         FORMAT "X(35)"
t_Cliente.num-coordinador      FORMAT ">>>>9"
t_Cliente.nom-coordinador      FORMAT "X(35)"
t_Cliente.importe-dv           FORMAT ">>,>>>,>>>,>>>.99":U 
t_Cliente.importe-mn           FORMAT ">>,>>>,>>>,>>>.99":U   
t_Cliente.Ingreso-Generado     FORMAT "->>>,>>>,>>>.99":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS MULTIPLE NO-VALIDATE SIZE 156.43 BY 12.65
         BGCOLOR 15 FGCOLOR 0 FONT 4 ROW-HEIGHT-CHARS .62 FIT-LAST-COLUMN.

DEFINE BROWSE brw_Contratos
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brw_Contratos C-Win _FREEFORM
  QUERY brw_Contratos DISPLAY
      t-Contratos.A&o            FORMAT ">>>9":U   
t-Contratos.Num-Contratos  FORMAT ">>>,>>9":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS MULTIPLE NO-VALIDATE SIZE 45 BY 13.65
         BGCOLOR 15 FGCOLOR 9 FONT 4
         TITLE BGCOLOR 15 FGCOLOR 9 "Total Contratos Anuales" ROW-HEIGHT-CHARS .46 FIT-LAST-COLUMN.

DEFINE BROWSE brw_General
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brw_General C-Win _FREEFORM
  QUERY brw_General DISPLAY
      t_General.Titulo               FORMAT "X(10)":U NO-LABEL  
t_General.importe-dv           FORMAT "->>>>,>>>,>>>,>>>.99":U 
t_General.importe-mn           FORMAT "->>>>,>>>,>>>,>>>.99":U   
t_General.Ingreso-Generado     FORMAT "->>>>,>>>,>>>.99":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS MULTIPLE NO-VALIDATE SIZE 54.14 BY 3.35
         BGCOLOR 15 FGCOLOR 2 FONT 4
         TITLE BGCOLOR 15 FGCOLOR 2 "Totales Generales" ROW-HEIGHT-CHARS .46 FIT-LAST-COLUMN.

DEFINE BROWSE brw_listado
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brw_listado C-Win _FREEFORM
  QUERY brw_listado DISPLAY
      t_Deal.num-deal             FORMAT ">>>>>>>9":U   
t_Deal.cve-sucursal         FORMAT "X(3)":U                
t_Deal.num-cliente          FORMAT ">>>>>9":U        
t_Deal.nom-cliente          FORMAT "X(30)":U 
t_Deal.Codigo-Divisa        FORMAT "X(5)":U
t_Deal.Tipo-Operacion       FORMAT "X(10)":U 
t_Deal.Contraparte          FORMAT "X(10)":U
t_Deal.importe-dv           FORMAT ">>>,>>>,>>>.99":U 
t_Deal.importe-mn           FORMAT ">>>,>>>,>>>.99":U   
t_Deal.Ingreso-Generado     FORMAT "->>>,>>>,>>>.99":U  
t_Deal.fecha-operacion     
t_Deal.fecha-liquidacion
t_Deal.num-folio                                                         
t_Deal.tipo-deal            FORMAT "X(15)":U
t_Deal.q_dispersa           FORMAT "X(10)":U
t_Deal.estatus              FORMAT "X(8)":U
t_Deal.num-promotor         FORMAT ">>>9":U   
t_Deal.nom-promotor         FORMAT "X(40)":U
t_Deal.producto             FORMAT "X(15)":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS MULTIPLE NO-SCROLLBAR-VERTICAL NO-VALIDATE SIZE 155.29 BY 12.12
         BGCOLOR 15 FGCOLOR 9 FONT 4 ROW-HEIGHT-CHARS .58 FIT-LAST-COLUMN.

DEFINE BROWSE brw_Promotor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brw_Promotor C-Win _FREEFORM
  QUERY brw_Promotor DISPLAY
      t_Promotor.cve-sucursal         FORMAT "X(3)":U                
t_Promotor.num-promotor         FORMAT ">>>9":U        
t_Promotor.nom-promotor         FORMAT "X(50)":U  
t_Promotor.importe-dv           FORMAT ">>,>>>,>>>,>>>.99":U 
t_Promotor.importe-mn           FORMAT ">>,>>>,>>>,>>>.99":U   
t_Promotor.Ingreso-Generado     FORMAT "->>>,>>>,>>>.99":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS MULTIPLE NO-VALIDATE SIZE 89 BY 12.38
         BGCOLOR 15 FGCOLOR 1 FONT 4 ROW-HEIGHT-CHARS .46 FIT-LAST-COLUMN.

DEFINE BROWSE brw_Resumen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS brw_Resumen C-Win _FREEFORM
  QUERY brw_Resumen DISPLAY
      t_Resumen.A&o         FORMAT "9999":U                
t_Resumen.Mes         FORMAT "99":U        
t_Resumen.importe-dv           FORMAT ">>,>>>,>>>,>>>.99":U 
t_Resumen.importe-mn           FORMAT ">>,>>>,>>>,>>>.99":U   
t_Resumen.Ingreso-Generado     FORMAT "->>>,>>>,>>>.99":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH SEPARATORS MULTIPLE NO-VALIDATE SIZE 46 BY 13.38
         BGCOLOR 15 FGCOLOR 2 FONT 4
         TITLE BGCOLOR 15 FGCOLOR 2 "Operaciones Mensuales" ROW-HEIGHT-CHARS .46 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME FRAME-3
     brw_Promotor AT ROW 1.08 COL 1.29 WIDGET-ID 400
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.86 ROW 8.19
         SIZE 158 BY 14.52
         TITLE "Operaciones por Promotor" WIDGET-ID 1000.

DEFINE FRAME FRAME-2
     brw_Cliente AT ROW 1 COL 1.57 WIDGET-ID 300
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 2.86 ROW 8.15
         SIZE 158 BY 14.81
         TITLE "Operaciones por Cliente" WIDGET-ID 900.

DEFINE FRAME DEFAULT-FRAME
     btn_Reporte AT ROW 2.08 COL 120 WIDGET-ID 164
     cb-num-promotor AT ROW 1.27 COL 10.57 COLON-ALIGNED WIDGET-ID 8
     tgl-incluir-bajas AT ROW 1.27 COL 69.86 WIDGET-ID 12
     tgl-subalternos AT ROW 2.12 COL 70 WIDGET-ID 10
     FechaInicio AT ROW 3.31 COL 10.14 COLON-ALIGNED WIDGET-ID 156
     vDescFechaIni AT ROW 3.31 COL 20.86 COLON-ALIGNED NO-LABEL WIDGET-ID 158
     FechaFinal AT ROW 4.31 COL 10 COLON-ALIGNED WIDGET-ID 24
     vDescFechaFin AT ROW 4.31 COL 20.86 COLON-ALIGNED NO-LABEL WIDGET-ID 26
     btn_Actualiza AT ROW 3.42 COL 72.72 WIDGET-ID 148
     brw_General AT ROW 3.5 COL 93.14 WIDGET-ID 600
     Btn_Salir AT ROW 23.62 COL 148
     fi-folder1 AT ROW 7.38 COL 4 NO-LABEL
     fi-folder2 AT ROW 7.38 COL 25.86 COLON-ALIGNED NO-LABEL
     fi-folder3 AT ROW 7.42 COL 52.86 NO-LABEL
     fi-folder4 AT ROW 7.42 COL 77.72 COLON-ALIGNED NO-LABEL
     RECT-1 AT ROW 7.23 COL 3
     RECT-2 AT ROW 7.23 COL 27.14
     RECT-3 AT ROW 7.27 COL 52.14
     RECT-4 AT ROW 7.27 COL 79
     RECT-6 AT ROW 23.12 COL 3.43
     IMAGE-1 AT ROW 1.31 COL 149 WIDGET-ID 160
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1.29 ROW 1
         SIZE 163.43 BY 24.23
         FGCOLOR 9 FONT 4.

DEFINE FRAME FRAME-1
     brw_listado AT ROW 1.04 COL 1.43 WIDGET-ID 200
     btn_excel AT ROW 13.46 COL 141.57 WIDGET-ID 152
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3 ROW 8.19
         SIZE 158 BY 14.81
         TITLE "Detalle de Operaciones eDivisas" WIDGET-ID 100.

DEFINE FRAME FRAME-4
     brw_Resumen AT ROW 1.27 COL 28 WIDGET-ID 500
     brw_Contratos AT ROW 1.27 COL 75 WIDGET-ID 1200
     btn_contratos AT ROW 1.27 COL 121 WIDGET-ID 158
    WITH 1 DOWN KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 3 ROW 8.27
         SIZE 158 BY 14.72
         TITLE "Operaciones Mensuales / Contratos" WIDGET-ID 1100.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Window
   Compile into: Objetos
   Allow: Basic,Browse,DB-Fields,Window,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
IF SESSION:DISPLAY-TYPE = "GUI":U THEN
  CREATE WINDOW C-Win ASSIGN
         HIDDEN             = YES
         TITLE              = "MONITOR eDIVISAS"
         HEIGHT             = 24.23
         WIDTH              = 164.14
         MAX-HEIGHT         = 36.38
         MAX-WIDTH          = 274.29
         VIRTUAL-HEIGHT     = 36.38
         VIRTUAL-WIDTH      = 274.29
         RESIZE             = yes
         SCROLL-BARS        = no
         STATUS-AREA        = no
         BGCOLOR            = ?
         FGCOLOR            = ?
         KEEP-FRAME-Z-ORDER = yes
         THREE-D            = yes
         MESSAGE-AREA       = no
         SENSITIVE          = yes.
ELSE {&WINDOW-NAME} = CURRENT-WINDOW.
/* END WINDOW DEFINITION                                                */
&ANALYZE-RESUME



/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW C-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* REPARENT FRAME */
ASSIGN FRAME FRAME-1:FRAME = FRAME DEFAULT-FRAME:HANDLE.

/* SETTINGS FOR FRAME DEFAULT-FRAME
   FRAME-NAME Custom                                                    */
/* BROWSE-TAB brw_General btn_Actualiza DEFAULT-FRAME */
ASSIGN 
       brw_General:NUM-LOCKED-COLUMNS IN FRAME DEFAULT-FRAME     = 2
       brw_General:ALLOW-COLUMN-SEARCHING IN FRAME DEFAULT-FRAME = TRUE
       brw_General:COLUMN-RESIZABLE IN FRAME DEFAULT-FRAME       = TRUE.

/* SETTINGS FOR BUTTON btn_Reporte IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN fi-folder1 IN FRAME DEFAULT-FRAME
   ALIGN-L                                                              */
/* SETTINGS FOR FILL-IN fi-folder3 IN FRAME DEFAULT-FRAME
   ALIGN-L                                                              */
/* SETTINGS FOR TOGGLE-BOX tgl-subalternos IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN vDescFechaFin IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN vDescFechaIni IN FRAME DEFAULT-FRAME
   NO-ENABLE                                                            */
/* SETTINGS FOR FRAME FRAME-1
                                                                        */
/* BROWSE-TAB brw_listado 1 FRAME-1 */
ASSIGN 
       brw_listado:NUM-LOCKED-COLUMNS IN FRAME FRAME-1     = 2
       brw_listado:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-1 = TRUE
       brw_listado:COLUMN-RESIZABLE IN FRAME FRAME-1       = TRUE.

/* SETTINGS FOR FRAME FRAME-2
                                                                        */
/* BROWSE-TAB brw_Cliente 1 FRAME-2 */
ASSIGN 
       brw_Cliente:NUM-LOCKED-COLUMNS IN FRAME FRAME-2     = 2
       brw_Cliente:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-2 = TRUE
       brw_Cliente:COLUMN-RESIZABLE IN FRAME FRAME-2       = TRUE.

/* SETTINGS FOR FRAME FRAME-3
                                                                        */
/* BROWSE-TAB brw_Promotor 1 FRAME-3 */
ASSIGN 
       brw_Promotor:NUM-LOCKED-COLUMNS IN FRAME FRAME-3     = 2
       brw_Promotor:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-3 = TRUE
       brw_Promotor:COLUMN-RESIZABLE IN FRAME FRAME-3       = TRUE.

/* SETTINGS FOR FRAME FRAME-4
                                                                        */
/* BROWSE-TAB brw_Resumen 1 FRAME-4 */
/* BROWSE-TAB brw_Contratos brw_Resumen FRAME-4 */
ASSIGN 
       brw_Contratos:NUM-LOCKED-COLUMNS IN FRAME FRAME-4     = 2
       brw_Contratos:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-4 = TRUE
       brw_Contratos:COLUMN-RESIZABLE IN FRAME FRAME-4       = TRUE.

ASSIGN 
       brw_Resumen:NUM-LOCKED-COLUMNS IN FRAME FRAME-4     = 2
       brw_Resumen:ALLOW-COLUMN-SEARCHING IN FRAME FRAME-4 = TRUE
       brw_Resumen:COLUMN-RESIZABLE IN FRAME FRAME-4       = TRUE.

/* SETTINGS FOR BUTTON btn_contratos IN FRAME FRAME-4
   NO-ENABLE                                                            */
IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
THEN C-Win:HIDDEN = no.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brw_Cliente
/* Query rebuild information for BROWSE brw_Cliente
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH t_Cliente NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brw_Cliente */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brw_Contratos
/* Query rebuild information for BROWSE brw_Contratos
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH t-Contratos NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brw_Contratos */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brw_General
/* Query rebuild information for BROWSE brw_General
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH t_General NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brw_General */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brw_listado
/* Query rebuild information for BROWSE brw_listado
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH t_Deal NO-LOCK by t_Deal.num-deal INDEXED-REPOSITION .
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brw_listado */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brw_Promotor
/* Query rebuild information for BROWSE brw_Promotor
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH t_Promotor NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brw_Promotor */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE brw_Resumen
/* Query rebuild information for BROWSE brw_Resumen
     _START_FREEFORM
OPEN QUERY {&SELF-NAME} FOR EACH t_Resumen NO-LOCK INDEXED-REPOSITION.
     _END_FREEFORM
     _Query            is OPENED
*/  /* BROWSE brw_Resumen */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME C-Win
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON END-ERROR OF C-Win /* MONITOR eDIVISAS */
OR ENDKEY OF {&WINDOW-NAME} ANYWHERE DO:
  /* This case occurs when the user presses the "Esc" key.
     In a persistently run window, just ignore this.  If we did not, the
     application would exit. */
  IF THIS-PROCEDURE:PERSISTENT THEN RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL C-Win C-Win
ON WINDOW-CLOSE OF C-Win /* MONITOR eDIVISAS */
DO:
  /* This event will close the window and terminate the procedure.  */
  APPLY "CLOSE":U TO THIS-PROCEDURE.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brw_Cliente
&Scoped-define FRAME-NAME FRAME-2
&Scoped-define SELF-NAME brw_Cliente
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brw_Cliente C-Win
ON START-SEARCH OF brw_Cliente IN FRAME FRAME-2
DO:
    ColumnHandle = brw_Cliente:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN  "cve-sucursal"           THEN OPEN QUERY brw_Cliente FOR EACH t_Cliente NO-LOCK BY t_Cliente.cve-sucursal         INDEXED-REPOSITION.
        WHEN  "num-cliente"            THEN OPEN QUERY brw_Cliente FOR EACH t_Cliente NO-LOCK BY t_Cliente.num-cliente          INDEXED-REPOSITION.
        WHEN  "nom-cliente"            THEN OPEN QUERY brw_Cliente FOR EACH t_Cliente NO-LOCK BY t_Cliente.nom-cliente          INDEXED-REPOSITION.
        WHEN  "num-promotor"           THEN OPEN QUERY brw_Cliente FOR EACH t_Cliente NO-LOCK BY t_Cliente.num-promotor         INDEXED-REPOSITION.
        WHEN  "importe-dv"             THEN OPEN QUERY brw_Cliente FOR EACH t_Cliente NO-LOCK BY t_Cliente.importe-dv           INDEXED-REPOSITION.
        WHEN  "importe-mn"             THEN OPEN QUERY brw_Cliente FOR EACH t_Cliente NO-LOCK BY t_Cliente.importe-mn           INDEXED-REPOSITION.
        WHEN  "Ingreso-Generado"       THEN OPEN QUERY brw_Cliente FOR EACH t_Cliente NO-LOCK BY t_Cliente.Ingreso-Generado     INDEXED-REPOSITION.
    END CASE.
     
    ColumnHandle = {&SELF-NAME}:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN "Ingreso-Generado"    
            THEN DO:
                IF a-ascending[7] = TRUE THEN DO:
                    a-ascending[7] = FALSE.
                    brw_Cliente:SET-SORT-ARROW(7,a-ascending[7]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Cliente NO-LOCK BY t_Cliente.Ingreso-Generado DESCENDING INDEXED-REPOSITION.
                END.
                ELSE DO:
                    a-ascending[7] = TRUE.
                    brw_Cliente:SET-SORT-ARROW(7,a-ascending[7]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Cliente NO-LOCK BY t_Cliente.Ingreso-Generado INDEXED-REPOSITION.
                END.
            END.
    END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brw_General
&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME brw_General
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brw_General C-Win
ON START-SEARCH OF brw_General IN FRAME DEFAULT-FRAME /* Totales Generales */
DO:
    ColumnHandle = brw_General:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN  "importe-dv"        THEN OPEN QUERY brw_General FOR EACH t_General NO-LOCK BY t_General.importe-dv           INDEXED-REPOSITION.
        WHEN  "importe-mn"        THEN OPEN QUERY brw_General FOR EACH t_General NO-LOCK BY t_General.importe-mn           INDEXED-REPOSITION.
        WHEN  "Ingreso-Generado"  THEN OPEN QUERY brw_General FOR EACH t_General NO-LOCK BY t_General.Ingreso-Generado     INDEXED-REPOSITION.
    END CASE. 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brw_listado
&Scoped-define FRAME-NAME FRAME-1
&Scoped-define SELF-NAME brw_listado
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brw_listado C-Win
ON START-SEARCH OF brw_listado IN FRAME FRAME-1
DO:
    ColumnHandle = brw_listado:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN  "Num-deal"               THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.num-deal             INDEXED-REPOSITION.
        WHEN  "Num-Folio"              THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.num-folio            INDEXED-REPOSITION.
        WHEN  "cve-sucursal"           THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.cve-sucursal         INDEXED-REPOSITION.
        WHEN  "num-cliente"            THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.num-cliente          INDEXED-REPOSITION.
        WHEN  "nom-cliente"            THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.nom-cliente          INDEXED-REPOSITION.
        WHEN  "Codigo-Divisa"          THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.Codigo-Divisa        INDEXED-REPOSITION.
        WHEN  "Tipo-Operacion"         THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.Tipo-Operacion       INDEXED-REPOSITION.
        WHEN  "Contraparte"            THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.Contraparte          INDEXED-REPOSITION.
        WHEN  "importe-dv"             THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.importe-dv           INDEXED-REPOSITION.
        WHEN  "importe-mn"             THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.importe-mn           INDEXED-REPOSITION.
        WHEN  "Ingreso-Generado"       THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.Ingreso-Generado     INDEXED-REPOSITION.
        WHEN  "fecha-operacion"        THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.fecha-operacion      INDEXED-REPOSITION. 
        WHEN  "fecha-liquidacion"      THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.fecha-liquidacion    INDEXED-REPOSITION. 
        WHEN  "tipo-deal"              THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.tipo-deal            INDEXED-REPOSITION. 
        WHEN  "num-promotor"           THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.num-promotor         INDEXED-REPOSITION. 
        WHEN  "nom-promotor"           THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.nom-promotor         INDEXED-REPOSITION. 
        WHEN  "producto"               THEN OPEN QUERY brw_listado FOR EACH t_Deal NO-LOCK BY t_Deal.producto             INDEXED-REPOSITION.
    END CASE. 
    
    ColumnHandle = {&SELF-NAME}:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN "Ingreso-Generado"    
            THEN DO:
                IF a-ascending[7] = TRUE THEN DO:
                    a-ascending[7] = FALSE.
                    brw_listado:SET-SORT-ARROW(7, a-ascending[7]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Deal NO-LOCK BY t_Deal.Ingreso-Generado DESCENDING INDEXED-REPOSITION.
                END.
                ELSE DO:
                    a-ascending[7] = TRUE.
                    brw_listado:SET-SORT-ARROW(7, a-ascending[7]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Deal NO-LOCK BY t_Deal.Ingreso-Generado INDEXED-REPOSITION.
                END.
            END.
    END CASE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brw_Promotor
&Scoped-define FRAME-NAME FRAME-3
&Scoped-define SELF-NAME brw_Promotor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brw_Promotor C-Win
ON START-SEARCH OF brw_Promotor IN FRAME FRAME-3
DO:
    ColumnHandle = brw_Promotor:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN  "cve-sucursal"           THEN OPEN QUERY brw_Promotor FOR EACH t_Promotor NO-LOCK BY t_Promotor.cve-sucursal         INDEXED-REPOSITION.
        WHEN  "num-promotor"           THEN OPEN QUERY brw_Promotor FOR EACH t_Promotor NO-LOCK BY t_Promotor.num-promotor         INDEXED-REPOSITION.
        WHEN  "nom-promotor"           THEN OPEN QUERY brw_Promotor FOR EACH t_Promotor NO-LOCK BY t_Promotor.nom-promotor         INDEXED-REPOSITION.
        WHEN  "importe-dv"             THEN OPEN QUERY brw_Promotor FOR EACH t_Promotor NO-LOCK BY t_Promotor.importe-dv           INDEXED-REPOSITION.
        WHEN  "importe-mn"             THEN OPEN QUERY brw_Promotor FOR EACH t_Promotor NO-LOCK BY t_Promotor.importe-mn           INDEXED-REPOSITION.
        WHEN  "Ingreso-Generado"       THEN OPEN QUERY brw_Promotor FOR EACH t_Promotor NO-LOCK BY t_Promotor.Ingreso-Generado     INDEXED-REPOSITION.
    END CASE. 
    
    ColumnHandle = {&SELF-NAME}:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN "Ingreso-Generado"    
            THEN DO:
                IF a-ascending[6] = TRUE THEN DO:
                    a-ascending[6] = FALSE.
                    brw_Promotor:SET-SORT-ARROW(6,a-ascending[6]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Promotor NO-LOCK BY t_Promotor.Ingreso-Generado DESCENDING INDEXED-REPOSITION.
                END.
                ELSE DO:
                    a-ascending[6] = TRUE.
                    brw_Promotor:SET-SORT-ARROW(6,a-ascending[6]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Promotor NO-LOCK BY t_Promotor.Ingreso-Generado INDEXED-REPOSITION.
                END.
            END.
    END CASE.    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brw_Resumen
&Scoped-define FRAME-NAME FRAME-4
&Scoped-define SELF-NAME brw_Resumen
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL brw_Resumen C-Win
ON START-SEARCH OF brw_Resumen IN FRAME FRAME-4 /* Operaciones Mensuales */
DO:
    ColumnHandle = brw_Resumen:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN  "cve-sucursal"      THEN OPEN QUERY brw_Resumen FOR EACH t_Resumen NO-LOCK BY t_Resumen.A&o                  INDEXED-REPOSITION.
        WHEN  "num-Resumen"       THEN OPEN QUERY brw_Resumen FOR EACH t_Resumen NO-LOCK BY t_Resumen.Mes                  INDEXED-REPOSITION.
        WHEN  "importe-dv"        THEN OPEN QUERY brw_Resumen FOR EACH t_Resumen NO-LOCK BY t_Resumen.importe-dv           INDEXED-REPOSITION.
        WHEN  "importe-mn"        THEN OPEN QUERY brw_Resumen FOR EACH t_Resumen NO-LOCK BY t_Resumen.importe-mn           INDEXED-REPOSITION.
        WHEN  "Ingreso-Generado"  THEN OPEN QUERY brw_Resumen FOR EACH t_Resumen NO-LOCK BY t_Resumen.Ingreso-Generado     INDEXED-REPOSITION.
    END CASE. 
    
    ColumnHandle = {&SELF-NAME}:CURRENT-COLUMN.
    
    CASE ColumnHandle:NAME  : 
        WHEN "Ingreso-Generado"    
            THEN DO:
                IF a-ascending[5] = TRUE THEN DO:
                    a-ascending[5] = FALSE.
                    brw_Resumen:SET-SORT-ARROW(5,a-ascending[5]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Resumen NO-LOCK BY t_Resumen.Ingreso-Generado DESCENDING INDEXED-REPOSITION.
                END.
                ELSE DO:
                    a-ascending[5] = TRUE.
                    brw_Resumen:SET-SORT-ARROW(5,a-ascending[5]) IN FRAME {&FRAME-NAME}.
                    OPEN QUERY {&SELF-NAME} FOR EACH t_Resumen NO-LOCK BY t_Resumen.Ingreso-Generado INDEXED-REPOSITION.
                END.
            END.
    END CASE.    

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME btn_Actualiza
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Actualiza C-Win
ON CHOOSE OF btn_Actualiza IN FRAME DEFAULT-FRAME /* Actualiza */
DO:
   RUN pro_actualiza.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-4
&Scoped-define SELF-NAME btn_contratos
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_contratos C-Win
ON CHOOSE OF btn_contratos IN FRAME FRAME-4 /* Contratos */
DO:
  /*Obtiene Informacion */
  SESSION:SET-WAIT-STATE("General":U). 
  RUN Contratos-Edivisas-1.p(OUTPUT TABLE t-Contratos).
  SESSION:SET-WAIT-STATE("":U). 
  {&OPEN-QUERY-brw_Contratos}
  
   APPLY 'VALUE-CHANGED' TO brw_Contratos IN FRAME FRAME-4.
 
  ENABLE brw_Contratos /*btn_Contratos*/ WITH FRAME FRAME-4.

    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME FRAME-1
&Scoped-define SELF-NAME btn_excel
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_excel C-Win
ON CHOOSE OF btn_excel IN FRAME FRAME-1 /* Excel */
DO:
    SESSION:SET-WAIT-STATE("General":U). 
    CREATE "Excel.APPLICATION" chExcelApplication.
    chWorkbook = chExcelApplication:Workbooks:ADD().
    chWorkSheet = chExcelApplication:Sheets:ITEM(1).
    chWorkSheet:NAME = "Autorizaciones". 
    ASSIGN
    chWorkSheet:COLUMNS("A"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("B"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("C"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("D"):ColumnWidth  = 40            
    chWorkSheet:COLUMNS("E"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("F"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("G"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("H"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("I"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("J"):ColumnWidth  = 15
    chWorkSheet:COLUMNS("K"):ColumnWidth  = 15
    chWorkSheet:COLUMNS("L"):ColumnWidth  = 15            
    chWorkSheet:COLUMNS("M"):ColumnWidth  = 40
    chWorkSheet:COLUMNS("N"):ColumnWidth  = 40
       
    chWorkSheet:Range("A1:E5"):FONT:Bold = TRUE
    chWorkSheet:Range("D4"):VALUE = "Reporte de EDivisas"
    chWorkSheet:Range("A1:AE5"):FONT:Bold = TRUE

    chWorkSheet:Range("A5"):VALUE  = "Num-deal"             
    chWorkSheet:Range("B5"):VALUE  = "Cve-sucursal"         
    chWorkSheet:Range("C5"):VALUE  = "Num-cliente"          
    chWorkSheet:Range("D5"):VALUE  = "Nom-cliente"          
    chWorkSheet:Range("E5"):VALUE  = "Importe-dv"           
    chWorkSheet:Range("F5"):VALUE  = "Importe-mn"           
    chWorkSheet:Range("G5"):VALUE  = "Ingreso-Generado"        
    chWorkSheet:Range("H5"):VALUE  = "Fecha-Operacion"
    chWorkSheet:Range("I5"):VALUE  = "Fecha-Liquidacion"
    chWorkSheet:Range("J5"):VALUE  = "Folio EDivisas"             
    chWorkSheet:Range("K5"):VALUE  = "Tipo-Deal"        
    chWorkSheet:Range("L5"):VALUE  = "Num-Promotor"    
    chWorkSheet:Range("M5"):VALUE  = "Promotor"          
    chWorkSheet:Range("N5"):VALUE  = "Producto"          
    vrl_idx = 6.

    FOR EACH t_Deal.
        
        ASSIGN
        chWorkSheet:Range("A" + STRING(vrl_idx)):VALUE  = t_Deal.num-deal            
        chWorkSheet:Range("B" + STRING(vrl_idx)):VALUE  = t_Deal.cve-sucursal        
        chWorkSheet:Range("C" + STRING(vrl_idx)):VALUE  = t_Deal.num-cliente         
        chWorkSheet:Range("D" + STRING(vrl_idx)):VALUE  = t_Deal.nom-cliente         
        chWorkSheet:Range("E" + STRING(vrl_idx)):VALUE  = t_Deal.importe-dv          
        chWorkSheet:Range("F" + STRING(vrl_idx)):VALUE  = t_Deal.importe-mn  
        chWorkSheet:Range("G" + STRING(vrl_idx)):VALUE  = t_Deal.Ingreso-Generado  
        chWorkSheet:Range("H" + STRING(vrl_idx)):VALUE  = t_Deal.fecha-operacion 
        chWorkSheet:Range("I" + STRING(vrl_idx)):VALUE  = t_Deal.fecha-liquidacion 
        chWorkSheet:Range("J" + STRING(vrl_idx)):VALUE  = t_Deal.Num-Folio 
        chWorkSheet:Range("K" + STRING(vrl_idx)):VALUE  = t_Deal.tipo-deal    
        chWorkSheet:Range("L" + STRING(vrl_idx)):VALUE  = t_Deal.num-promotor          
        chWorkSheet:Range("M" + STRING(vrl_idx)):VALUE  = t_Deal.nom-promotor            
        chWorkSheet:Range("N" + STRING(vrl_idx)):VALUE  = t_Deal.Producto          
        chWorkSheet:Columns("E:G"):EntireColumn:NumberFormat = "#,###,###,##0.00".
        vrl_idx = vrl_idx + 1. 
    END.
    chExcelApplication:VISIBLE = TRUE. 
    RELEASE OBJECT chExcelApplication.      
    RELEASE OBJECT chWorksheet.
    SESSION:SET-WAIT-STATE("":U). 
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define FRAME-NAME DEFAULT-FRAME
&Scoped-define SELF-NAME btn_Reporte
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn_Reporte C-Win
ON CHOOSE OF btn_Reporte IN FRAME DEFAULT-FRAME /* Reporte Ingresos eDivisas */
DO:
    RUN pr-ejecutar-funcion("rpinged").
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Salir
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Salir C-Win
ON CHOOSE OF Btn_Salir IN FRAME DEFAULT-FRAME /* Salir */
DO:
  APPLY "CLOSE":U TO THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME cb-num-promotor
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL cb-num-promotor C-Win
ON VALUE-CHANGED OF cb-num-promotor IN FRAME DEFAULT-FRAME /* Promotor */
DO:

    ASSIGN FRAME DEFAULT-FRAME 
        cb-num-promotor
        tgl-incluir-bajas
        tgl-subalternos.
     
    RUN pr-verifica-jerarquia.
    
    ASSIGN v-num-promotor     = cb-num-promotor. 
    
    if v-num-promotor = 0 THEN 
    ASSIGN v-num-promotor-lis = "".
    ELSE ASSIGN v-num-promotor-lis = STRING(cb-num-promotor).
    
    RUN AbrirConsulta. 
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FechaFinal
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FechaFinal C-Win
ON LEAVE OF FechaFinal IN FRAME DEFAULT-FRAME /* Fecha Final */
DO:
  ASSIGN FechaFinal
         vDescFechaFin = CAPS({IN/fecha.i &fecha = FechaFinal &formato = Extendido}).
  DISPLAY vDescFechaFin WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FechaFinal C-Win
ON RETURN OF FechaFinal IN FRAME DEFAULT-FRAME /* Fecha Final */
DO:
  APPLY "TAB" TO SELF.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME FechaInicio
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FechaInicio C-Win
ON LEAVE OF FechaInicio IN FRAME DEFAULT-FRAME /* Fecha Inicio */
DO:
  ASSIGN FechaInicio
         vDescFechaIni = CAPS({IN/fecha.i &fecha = FechaInicio &formato = Extendido}).
  DISPLAY vDescFechaIni WITH FRAME {&FRAME-NAME}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL FechaInicio C-Win
ON RETURN OF FechaInicio IN FRAME DEFAULT-FRAME /* Fecha Inicio */
DO:
  APPLY "TAB" TO SELF.
  RETURN NO-APPLY.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-folder1
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-folder1 C-Win
ON MOUSE-SELECT-CLICK OF fi-folder1 IN FRAME DEFAULT-FRAME
DO:
  RUN pr-cambio-pagina(1).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-folder2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-folder2 C-Win
ON MOUSE-SELECT-CLICK OF fi-folder2 IN FRAME DEFAULT-FRAME
DO:
  RUN pr-cambio-pagina(2).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-folder3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-folder3 C-Win
ON MOUSE-SELECT-CLICK OF fi-folder3 IN FRAME DEFAULT-FRAME
DO:
  RUN pr-cambio-pagina(3).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME fi-folder4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL fi-folder4 C-Win
ON MOUSE-SELECT-CLICK OF fi-folder4 IN FRAME DEFAULT-FRAME
DO:
  RUN pr-cambio-pagina(4).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl-incluir-bajas
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl-incluir-bajas C-Win
ON VALUE-CHANGED OF tgl-incluir-bajas IN FRAME DEFAULT-FRAME /* Incluir pomotores de baja */
DO:
  ASSIGN tgl-incluir-bajas.
  RUN AbrirConsulta.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME tgl-subalternos
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL tgl-subalternos C-Win
ON VALUE-CHANGED OF tgl-subalternos IN FRAME DEFAULT-FRAME /* Todos sus subalternos */
DO:
    
    ASSIGN tgl-subalternos.
    ASSIGN FRAME DEFAULT-FRAME cb-num-promotor.
 
    IF tgl-subalternos THEN RUN pr_carga_subalternos.
    ELSE v-num-promotor-lis = STRING(V-NUM-PROMOTOR) .
    
    RUN AbrirConsulta.
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define BROWSE-NAME brw_Cliente
&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK C-Win 


/* ***************************  Main Block  *************************** */

/* Set CURRENT-WINDOW: this will parent dialog-boxes and frames.        */
ASSIGN CURRENT-WINDOW                = {&WINDOW-NAME} 
       THIS-PROCEDURE:CURRENT-WINDOW = {&WINDOW-NAME}.

/* The CLOSE event can be used from inside or outside the procedure to  */
/* terminate it.                                                        */
ON CLOSE OF THIS-PROCEDURE 
   RUN disable_UI.

/* Best default for GUI applications is...                              */
PAUSE 0 BEFORE-HIDE.

/* Now enable the interface and wait for the exit condition.            */
/* (NOTE: handle ERROR and END-KEY so cleanup code will always fire.    */
MAIN-BLOCK:
DO ON ERROR   UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK
   ON END-KEY UNDO MAIN-BLOCK, LEAVE MAIN-BLOCK:
  RUN enable_UI.
  
   &IF DEFINED(AppBuilder_is_Running) NE 0 &THEN
     FIND ScrUsr WHERE ScrUsr.s-userid = "erojas-mty":U NO-LOCK NO-ERROR.
     &ELSE
     FIND ScrUsr WHERE ScrUsr.s-userid = USERID("DICTDB":U) NO-LOCK NO-ERROR.
    &ENDIF

    FIND DtsGnr WHERE DtsGnr.cve-sucursal = ScrUsr.cve-sucursal NO-LOCK NO-ERROR.
    FIND Promotor WHERE Promotor.s-userid = ScrUsr.s-userid NO-LOCK NO-ERROR.


  RUN pr-asignar-secciones.
  RUN pr-datos-generales.
  RUN pr-cambio-pagina(1).
  RUN pr-consultar-pdcc.
  
  RUN pr-datos-iniciales.

  APPLY "ENTRY":U TO Btn_Actualiza IN FRAME {&FRAME-NAME}.
  IF NOT THIS-PROCEDURE:PERSISTENT THEN
    WAIT-FOR CLOSE OF THIS-PROCEDURE.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE AbrirConsulta C-Win 
PROCEDURE AbrirConsulta :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE v-num-opr AS ROWID NO-UNDO.

  EMPTY TEMP-TABLE TT-JerarquiaPromocion.
  IF NOT vb-conJerarquia THEN DO:
      CREATE TT-JerarquiaPromocion.
      ASSIGN TT-JerarquiaPromocion.num-promotor = v-num-promotor.
  END.
  ELSE
      FOR EACH JerarquiaPromocion
          WHERE (JerarquiaPromocion.promotor-subdirector = v-cve-subdirector
             OR v-cve-subdirector = 0)
            AND ( ( IF tgl-subalternos = TRUE THEN
                     JerarquiaPromocion.promotor-subdirector = v-num-promotor
                  ELSE
                     JerarquiaPromocion.num-promotor = v-num-promotor)
                OR v-num-promotor = 0 )
          NO-LOCK:
            CREATE TT-JerarquiaPromocion.
            ASSIGN TT-JerarquiaPromocion.num-promotor = JerarquiaPromocion.num-promotor.
      END.
  
   APPLY "CHOOSE":U TO btn_Actualiza IN FRAME DEFAULT-FRAME .
    
  {&OPEN-QUERY-brw_listado}
  {&OPEN-QUERY-brw_Cliente}
  {&OPEN-QUERY-brw_Promotor}
  {&OPEN-QUERY-brw_Resumen}
  {&OPEN-QUERY-brw_General} 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI C-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Delete the WINDOW we created */
  IF SESSION:DISPLAY-TYPE = "GUI":U AND VALID-HANDLE(C-Win)
  THEN DELETE WIDGET C-Win.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI C-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY cb-num-promotor tgl-incluir-bajas tgl-subalternos FechaInicio 
          vDescFechaIni FechaFinal vDescFechaFin fi-folder1 fi-folder2 
          fi-folder3 fi-folder4 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  ENABLE cb-num-promotor tgl-incluir-bajas FechaInicio FechaFinal btn_Actualiza 
         brw_General Btn_Salir fi-folder1 fi-folder2 fi-folder3 fi-folder4 
         RECT-1 RECT-2 RECT-3 RECT-4 RECT-6 IMAGE-1 
      WITH FRAME DEFAULT-FRAME IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-DEFAULT-FRAME}
  ENABLE brw_Cliente 
      WITH FRAME FRAME-2 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-2}
  ENABLE brw_Promotor 
      WITH FRAME FRAME-3 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-3}
  ENABLE brw_listado btn_excel 
      WITH FRAME FRAME-1 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-1}
  ENABLE brw_Resumen brw_Contratos 
      WITH FRAME FRAME-4 IN WINDOW C-Win.
  {&OPEN-BROWSERS-IN-QUERY-FRAME-4}
  VIEW C-Win.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-asignar-secciones C-Win 
PROCEDURE pr-asignar-secciones :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DO WITH FRAME DEFAULT-FRAME:
    ASSIGN fi-folder1:SCREEN-VALUE = "Detalle de Operaciones" 
           fi-folder2:SCREEN-VALUE = "Operaciones Clientes"
           fi-folder3:SCREEN-VALUE = "Operaciones Promotor"
           fi-folder4:SCREEN-VALUE = "Operaciones Mensuales / Contratos" .
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-cambio-pagina C-Win 
PROCEDURE pr-cambio-pagina :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-pagina AS INTEGER NO-UNDO.

  ASSIGN v-cual-pagina = p-pagina
         FRAME FRAME-1:HIDDEN = p-pagina <> 1
         FRAME FRAME-2:HIDDEN = p-pagina <> 2
         FRAME FRAME-3:HIDDEN = p-pagina <> 3
         FRAME FRAME-4:HIDDEN = p-pagina <> 4.
  
  ASSIGN fi-folder1:BGCOLOR IN FRAME {&FRAME-NAME} = (IF p-pagina = 1 THEN 8 ELSE ?)
         fi-folder2:BGCOLOR IN FRAME {&FRAME-NAME} = (IF p-pagina = 2 THEN 8 ELSE ?)
         fi-folder3:BGCOLOR IN FRAME {&FRAME-NAME} = (IF p-pagina = 3 THEN 8 ELSE ?)
         fi-folder4:BGCOLOR IN FRAME {&FRAME-NAME} = (IF p-pagina = 4 THEN 8 ELSE ?)
         
         rect-1:EDGE-PIXELS IN FRAME {&FRAME-NAME} = (IF p-pagina = 1 THEN 1000 ELSE 2)
         rect-2:EDGE-PIXELS IN FRAME {&FRAME-NAME} = (IF p-pagina = 2 THEN 1000 ELSE 2)
         rect-3:EDGE-PIXELS IN FRAME {&FRAME-NAME} = (IF p-pagina = 3 THEN 1000 ELSE 2)
         rect-4:EDGE-PIXELS IN FRAME {&FRAME-NAME} = (IF p-pagina = 4 THEN 1000 ELSE 2).
         
  /*CASE p-pagina:
      WHEN 1 THEN DO:
         ENABLE ALL WITH FRAME FRAME-1.
         DISABLE ALL WITH FRAME FRAME-2. 
         DISABLE ALL WITH FRAME FRAME-3.
         DISABLE ALL WITH FRAME FRAME-4.
      END.
      WHEN 2 THEN DO:
         ENABLE ALL WITH FRAME FRAME-2.
         DISABLE ALL WITH FRAME FRAME-1.
         DISABLE ALL WITH FRAME FRAME-3.
         DISABLE ALL WITH FRAME FRAME-4.
      END.
      WHEN 3 THEN DO:
         ENABLE ALL WITH FRAME FRAME-3.
         DISABLE ALL WITH FRAME FRAME-1.
         DISABLE ALL WITH FRAME FRAME-2.
         DISABLE ALL WITH FRAME FRAME-4.
      END.      
      WHEN 4 THEN DO:
/*         ENABLE ALL WITH FRAME FRAME-4.*/
         ENABLE brw_Contratos brw_Contratos WITH FRAME FRAME-4. 
         DISABLE ALL WITH FRAME FRAME-1.
         DISABLE ALL WITH FRAME FRAME-2. 
         DISABLE ALL WITH FRAME FRAME-3.
      END.      
      
  END CASE.    
*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-carga-promotores C-Win 
PROCEDURE pr-carga-promotores :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE v-con-restriccion AS LOGICAL NO-UNDO.
  DEFINE VARIABLE v-coordinador     AS LOGICAL NO-UNDO. 
  DEFINE VARIABLE v-region          AS INTEGER NO-UNDO.
  DEFINE VARIABLE v-cadena          AS CHARACTER  NO-UNDO.
  DEFINE VARIABLE vPromSub          AS INTEGER NO-UNDO.

  DEFINE BUFFER b-jerarquia FOR JerarquiaPromocion.

  ASSIGN cb-num-promotor:SENSITIVE IN FRAME {&FRAME-NAME} = TRUE.
  
    FIND bf-Promotor WHERE bf-Promotor.s-userid = ScrUsr.s-userid NO-LOCK NO-ERROR.
    FIND FIRST JerarquiaPromocion NO-LOCK NO-ERROR.
    
    
    IF NOT AVAILABLE bf-Promotor OR (AVAILABLE bf-Promotor AND bf-Promotor.operador = TRUE) OR 
                                              JerarquiaPromocion.s-userid-director = USERID("DICTDB")
    THEN DO:  
        ASSIGN vb-conJerarquia = TRUE.
        ASSIGN btn_contratos:SENSITIVE IN FRAME FRAME-4     = TRUE 
        btn_reporte:SENSITIVE IN FRAME {&FRAME-NAME}        = TRUE .
        RUN pr-SinRestriccion.
    END.
    ELSE DO:
        ASSIGN btn_contratos:SENSITIVE IN FRAME FRAME-4     = FALSE 
               btn_reporte:SENSITIVE IN FRAME {&FRAME-NAME} = FALSE .
    
        cb-num-promotor:LIST-ITEM-PAIRS IN FRAME {&FRAME-NAME} = ?.        
        FIND FIRST JerarquiaPromocion WHERE JerarquiaPromocion.num-promotor = bf-Promotor.num-promotor NO-LOCK NO-ERROR.
        IF NOT AVAILABLE JerarquiaPromocion THEN DO:
            
            vb-conJerarquia = FALSE.
            cb-num-promotor:ADD-LAST(STRING(bf-Promotor.num-promotor,">>>9") 
                           + "  " + CAPS(bf-Promotor.nombre-promotor),bf-Promotor.num-promotor)
                           IN FRAME DEFAULT-FRAME. 
            /*cb-num-promotor:ADD-LAST(CAPS(Promotor.nombre-promotor) + "-" + STRING(Promotor.num-promotor,">>>9"),Promotor.num-promotor). */
                           
        END.
        ELSE
            ASSIGN vb-conJerarquia = TRUE
                   cb-num-promotor:LIST-ITEM-PAIRS = ?
                   vPromSub = JerarquiaPromocion.promotor-subdirector.

        /* Si el usuario firmante es un promotor, se verifica su jerarqu�a */
        FOR EACH JerarquiaPromocion WHERE JerarquiaPromocion.promotor-subdirector = vPromSub NO-LOCK
                                    USE-INDEX idxJerarquiaPromocion
                                    BREAK BY VECTODIV.JerarquiaPromocion.s-userid-director:
            FIND Promotor WHERE Promotor.num-promotor = JerarquiaPromocion.num-promotor 
                            /*AND (IF tgl-incluir-bajas THEN TRUE ELSE Promotor.centro-costo <> 0)*/
                            AND (IF tgl-incluir-bajas THEN TRUE ELSE Promotor.estado-actual = TRUE)
                                NO-LOCK NO-ERROR.
                                                                                                 
            IF AVAILABLE Promotor THEN
                cb-num-promotor:ADD-LAST(STRING(Promotor.num-promotor,">>>9") 
                                   + "  " + CAPS(Promotor.nombre-promotor),Promotor.num-promotor). 
                                   
        /* cb-num-promotor:ADD-LAST(CAPS(Promotor.nombre-promotor) + "-" + STRING(Promotor.num-promotor,">>>9"),Promotor.num-promotor). */
        END.
    END.       
      
    IF AVAILABLE bf-Promotor THEN
        cb-num-promotor:SCREEN-VALUE = ENTRY(LOOKUP(STRING(bf-Promotor.num-promotor,'>>>9'),cb-num-promotor:LIST-ITEM-PAIRS),cb-num-promotor:LIST-ITEM-PAIRS).
    ELSE
        cb-num-promotor:SCREEN-VALUE = ENTRY(2,cb-num-promotor:LIST-ITEM-PAIRS).
     RUN pr-verifica-jerarquia.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-consultar-pdcc C-Win 
PROCEDURE pr-consultar-pdcc :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  ASSIGN FRAME {&FRAME-NAME}
         FechaInicio
         FechaFinal.
 
  {&OPEN-QUERY-brw_listado}
  {&OPEN-QUERY-brw_Cliente}
  {&OPEN-QUERY-brw_Promotor}
  {&OPEN-QUERY-brw_Resumen}
  {&OPEN-QUERY-brw_General}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-datos-generales C-Win 
PROCEDURE pr-datos-generales :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  FIND DtsGnr WHERE DtsGnr.cve-sucursal = "MTY":U NO-LOCK NO-ERROR.
  ASSIGN FechaInicio = DtsGnr.fecha-operacion.
  ASSIGN vDescFechaIni = CAPS({IN/fecha.i &fecha = FechaInicio &formato = Extendido}).
  
  ASSIGN FechaFinal  = DtsGnr.fecha-operacion.
  ASSIGN vDescFechaFin = CAPS({IN/fecha.i &fecha = FechaFinal &formato = Extendido}).

  DISPLAY FechaInicio  FechaFinal  vDescFechaIni vDescFechaFin
          WITH FRAME {&FRAME-NAME}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-datos-iniciales C-Win 
PROCEDURE pr-datos-iniciales :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/  
  RUN pr-carga-promotores.
 
  FIND VectoDiv.Promotor WHERE VectoDiv.Promotor.s-userid = USERID("DICTDB":U) NO-LOCK NO-ERROR. 


  FIND VectoDiv.DtsGnr WHERE VectoDiv.DtsGnr.cve-sucursal = VectoDiv.Promotor.cve-sucursal NO-LOCK NO-ERROR.
  IF NOT AVAILABLE VectoDiv.DtsGnr THEN FIND VectoDiv.DtsGnr WHERE VectoDiv.DtsGnr.cve-sucursal = "MTY":U NO-LOCK NO-ERROR.
    
  ASSIGN BROWSE {&BROWSE-NAME}:NUM-LOCKED-COLUMNS = 5
         v-num-promotor = (IF AVAILABLE VectoDiv.Promotor THEN VectoDiv.Promotor.num-promotor ELSE 0)
         v-num-promotor-lis = (IF AVAILABLE VectoDiv.Promotor THEN STRING(VectoDiv.Promotor.num-promotor) ELSE "")
         v-es-promotor = (IF AVAILABLE VectoDiv.Promotor THEN VectoDiv.Promotor.operador = FALSE ELSE FALSE).
  
  RUN AbrirConsulta.  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-ejecutar-funcion C-Win 
PROCEDURE pr-ejecutar-funcion :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE INPUT PARAMETER ip-funcion LIKE Std.SysFuncion.s-id-funcion NO-UNDO.

  DEFINE VARIABLE v-programa  AS CHARACTER NO-UNDO.
  DEFINE VARIABLE v-ejecucion AS CHARACTER NO-UNDO.
  DEFINE VARIABLE v-ventana-creada AS WIDGET-HANDLE NO-UNDO.


  FIND Std.SysFuncion WHERE Std.SysFuncion.s-id-funcion = ip-funcion NO-LOCK NO-ERROR.
  IF NOT AVAILABLE Std.SysFuncion THEN DO:
    FIND Std.SysAreaTrabajo WHERE Std.SysAreaTrabajo.s-id-area = ip-funcion NO-LOCK NO-ERROR.
    IF NOT AVAILABLE Std.SysAreaTrabajo THEN DO:
      IF ip-funcion <> ? THEN RETURN ERROR.
    END.
    ELSE 
      ASSIGN v-programa = Std.SysAreaTrabajo.s-programa.
  END.
  ELSE
    ASSIGN v-programa = Std.SysFuncion.s-programa.
    
  ASSIGN v-ejecucion = REPLACE(v-programa,".p":U,"":U)
         v-ejecucion = REPLACE(v-ejecucion,".w":U,"":U).  
  
  ASSIGN {&WINDOW-NAME}:HIDDEN = TRUE.


  IF AVAILABLE Std.SysFuncion THEN DO:
    FIND Std.SysTpoFnc OF Std.SysFuncion NO-LOCK NO-ERROR.
    
    IF std.SysFuncion.s-tipo-funcion = "REP":U /* reporte */ THEN DO:
      FIND Std.SysReporte OF Std.SysFuncion NO-LOCK NO-ERROR.
      IF AVAILABLE Std.SysReporte THEN DO:              
        IF Std.SysReporte.s-preview THEN DO:
          RUN pr/reprtstr.p (Std.SysReporte.s-id-funcion).
        END.
        ELSE                                                
          RUN pr/selout.w (Std.SysReporte.s-id-funcion,FALSE).
      END.
      ELSE    
        MESSAGE "No fu� posible ejecutar al programa de la funci�n de reporte."
                VIEW-AS ALERT-BOX ERROR BUTTONS OK.
      END.
    ELSE /* no es reporte */ 
      IF std.SysTpoFnc.s-disparador <> ? THEN 
        RUN VALUE(std.SysTpoFnc.s-disparador) (Std.SysFuncion.s-id-funcion).
      ELSE
        IF SEARCH(v-programa) <> ? OR SEARCH(v-ejecucion + ".r":U) <> ? THEN
          RUN VALUE(v-programa) NO-ERROR.
        ELSE
          MESSAGE "No se encontr� al programa a ejecutar." VIEW-AS ALERT-BOX ERROR.
  END. /* Funciones */
  ELSE
  IF AVAILABLE Std.SysAreaTrabajo THEN
    IF SEARCH(v-programa) <> ? OR SEARCH(v-ejecucion + ".r":U) <> ? THEN
      RUN VALUE(v-programa) NO-ERROR.
    ELSE
      MESSAGE "No se encontr� al programa a ejecutar." VIEW-AS ALERT-BOX ERROR.
  
  ASSIGN {&WINDOW-NAME}:HIDDEN = FALSE.

  IF ERROR-STATUS:ERROR THEN RETURN ERROR.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-SinRestriccion C-Win 
PROCEDURE pr-SinRestriccion :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    ASSIGN cb-num-promotor:LIST-ITEM-PAIRS IN FRAME DEFAULT-FRAME = ?.
    
    cb-num-promotor:ADD-LAST("TODOS",0).
    FOR EACH Promotor WHERE Promotor.centro-costo <> 0                                                
                        AND (IF tgl-incluir-bajas THEN TRUE ELSE Promotor.estado-actual = TRUE)
                      NO-LOCK By Promotor.nombre:   

        /*cb-num-promotor:ADD-LAST(STRING(Promotor.num-promotor,">>>9") 
                       + "  " + CAPS(Promotor.nombre-promotor),Promotor.num-promotor)
                       IN FRAME DEFAULT-FRAME.   */
                       
        cb-num-promotor:ADD-LAST(CAPS(Promotor.nombre-promotor) + " - " + STRING(Promotor.num-promotor,">>>9"),Promotor.num-promotor).
    END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr-verifica-jerarquia C-Win 
PROCEDURE pr-verifica-jerarquia :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    
    ASSIGN tgl-subalternos:CHECKED IN FRAME DEFAULT-FRAME = FALSE.
    
    /* Verifica si el promotor seleccionado es tambien un sub-director */
    FIND FIRST bf-Jerarquia WHERE bf-Jerarquia.promotor-subdirector = INTEGER (INPUT FRAME DEFAULT-FRAME cb-num-promotor)
                            USE-INDEX IdxSubdirector NO-LOCK NO-ERROR.

    IF AVAILABLE bf-Jerarquia THEN tgl-subalternos:SENSITIVE IN FRAME DEFAULT-FRAME = TRUE.
    ELSE tgl-subalternos:SENSITIVE IN FRAME DEFAULT-FRAME = FALSE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pro_actualiza C-Win 
PROCEDURE pro_actualiza :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  /*Obtiene Informacion */ 

  SESSION:SET-WAIT-STATE("General":U). 
  
  RUN ActOpEDivisasConsulta2.p(INPUT FechaInicio, INPUT FechaFinal, v-num-promotor-lis,
                      OUTPUT TABLE t_Deal, OUTPUT TABLE t_Cliente, OUTPUT TABLE t_Promotor, OUTPUT TABLE t_Resumen, OUTPUT TABLE t_General).
                      
  SESSION:SET-WAIT-STATE("":U). 
  {&OPEN-QUERY-brw_listado}
  {&OPEN-QUERY-brw_Cliente}
  {&OPEN-QUERY-brw_Promotor}
  {&OPEN-QUERY-brw_Resumen}
  {&OPEN-QUERY-brw_General}

  DISABLE ALL WITH FRAME DEFAUL-FRAME.
  ENABLE FechaInicio FechaFinal btn_salir btn_actualiza WITH FRAME DEFAULT-FRAME. 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE pr_carga_subalternos C-Win 
PROCEDURE pr_carga_subalternos :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE VARIABLE v-todos-promotores      AS LOGICAL NO-UNDO.
    DEFINE VARIABLE v-solo-promotor         AS LOGICAL NO-UNDO.
    DEFINE VARIABLE v-solo-grupo            AS LOGICAL NO-UNDO.
    DEFINE VARIABLE v-promotor              AS INTEGER NO-UNDO.
    DEFINE VARIABLE v-coordinador           AS INTEGER NO-UNDO.
    DEFINE VARIABLE v-noencontrado-prom     AS LOGICAL NO-UNDO.
    DEFINE VARIABLE vc-promotor             AS CHARACTER NO-UNDO.

    DEFINE BUFFER b-jerarquia FOR JerarquiaPromocion.
        
    v-num-promotor-lis  = "".
    for each JerarquiaPromocion where JerarquiaPromocion.promotor-subdirector = cb-num-promotor no-lock:
        find promotor where promotor.num-promotor = jerarquiaPromocion.num-promotor and 
                         promotor.estado-actual = true no-lock no-error.
        if available promotor then assign v-num-promotor-lis  = v-num-promotor-lis  + string(JerarquiaPromocion.num-promotor) + ",".
    end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ModificacionesNQNUEVOPRESENTACION */
